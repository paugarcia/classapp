package com.example.particlesclasse.ui

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.example.particlesclasse.R
import com.example.particlesclasse.data.Particle
import com.example.particlesclasse.data.ParticleEditActivity
import com.example.particlesclasse.data.ParticleEditActivity.Companion.EXTRA_PARTICLE_ID
import com.example.particlesclasse.data.Particles

class ParticlesListAdapter(val context: Context, val particles: ArrayList<Particle>) :
    RecyclerView.Adapter<ParticlesListAdapter.ViewHolder>() {

    private var lastClick: Int? = null

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.particle_name)
        val image: ImageView = view.findViewById(R.id.particle_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.particle_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val particle = particles[position]

        holder.name.text = particle.name

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        if (prefs.getBoolean("color_particles", true)) {
            val color = when (particle.family) {
                Particle.Family.QUARK -> R.color.quarks
                Particle.Family.LEPTON -> R.color.leptons
                Particle.Family.GAUGE_BOSON -> R.color.gauge_bosons
                Particle.Family.SCALAR_BOSON -> R.color.higgs
                Particle.Family.ANTIPARTICLE -> R.color.design_default_color_primary_dark
            }

            holder.image.setColorFilter(context.getColor(color))
        }

        holder.view.setOnClickListener {
            lastClick = position
            val intent = Intent(context, ParticleEditActivity::class.java)
            intent.putExtra(EXTRA_PARTICLE_ID, position)
            context.startActivity(intent)
        }
    }

    fun updateOnlyLastClick() {
        notifyItemChanged(lastClick ?: return)
    }

    override fun getItemCount(): Int {
        return particles.size
    }

}