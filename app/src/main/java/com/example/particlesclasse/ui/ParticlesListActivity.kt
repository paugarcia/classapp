package com.example.particlesclasse.ui

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.example.particlesclasse.R
import com.example.particlesclasse.data.Particle
import com.example.particlesclasse.data.Particles
import com.example.particlesclasse.data.Persistence
import com.example.particlesclasse.databinding.FragmentParticlesListBinding
import com.google.android.material.chip.Chip
import java.io.IOException
import java.io.ObjectInputStream
import kotlin.reflect.safeCast

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ParticlesListActivity : AppCompatActivity() {

    private lateinit var binding: FragmentParticlesListBinding
    private var shownParticles = ArrayList<Particle>()
    private val filters = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = FragmentParticlesListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Read particles file
        Persistence(this).readData()

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val extraParticles = prefs.getStringSet("extra_particles", HashSet())

        extraParticles?.forEach {
            Particles.add(Particle(it, Particle.Family.ANTIPARTICLE))
        }

        val newArray = Particles.distinctBy { it.name }
        Particles.clear()
        Particles.addAll(newArray)


        // Implement views
        shownParticles = ArrayList(Particles)
        binding.particlesList.adapter = ParticlesListAdapter(this, shownParticles)

        val particlesList = Particles.map { it.name } + Particle.Family.values().map { it.name }

        binding.searchBar.setAdapter(
            ArrayAdapter(
                this,
                R.layout.item_list_text,
                particlesList
            )
        )
        binding.searchBar.setOnItemClickListener { parent, view, position, id ->
            // Obtenim el text de l'element seleccionat a la llista
            val selection = parent.getItemAtPosition(position).toString()

            val chip = Chip(this)
            chip.text = selection
            chip.chipIcon =
                ContextCompat.getDrawable(this, R.drawable.ic_baseline_cancel_24)

            chip.setOnClickListener {
                binding.chipGroup.removeView(it)
                filters.remove(selection)
                filterListAndRefresh()
            }

            filters.add(selection)
            binding.chipGroup.addView(chip)
            binding.searchBar.text.clear()

            filterListAndRefresh()
        }
    }


    override fun onResume() {
        super.onResume()
        ParticlesListAdapter::class.safeCast(binding.particlesList.adapter)?.updateOnlyLastClick()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun filterListAndRefresh() {
        shownParticles.clear()

        filters.forEach { filter ->
            shownParticles.addAll(Particles.filter { it.name == filter })
            shownParticles.addAll(Particles.filter { it.family.name == filter })
        }

        if (shownParticles.isEmpty()) {
            shownParticles.addAll(Particles)
        }

        binding.particlesList.adapter?.notifyDataSetChanged()
    }

}