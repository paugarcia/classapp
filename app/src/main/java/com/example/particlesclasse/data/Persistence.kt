package com.example.particlesclasse.data

import android.content.Context
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

class Persistence(val context: Context) {

    fun saveData() {
        context.openFileOutput(Particles.PARTICLES_FILENAME, Context.MODE_PRIVATE).use { io ->
            ObjectOutputStream(io).use {
                it.writeObject(Particles)
            }
        }
    }

    fun readData() {
        val particles = try {
            context.openFileInput(Particles.PARTICLES_FILENAME).use { io ->
                ObjectInputStream(io).use {
                    it.readObject() as Particles
                }
            }
        } catch (e: IOException) {
            null
        }

        if (particles == null) {
            Particles.resetData()
        } else {
            Particles.clear()
            Particles.addAll(particles)
        }
    }
}