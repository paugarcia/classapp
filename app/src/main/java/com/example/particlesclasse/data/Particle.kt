package com.example.particlesclasse.data

import java.io.Serializable
import kotlin.random.Random

data class Particle(
    var name: String,
    var family: Family,
    var mass: Double,
    var spin: Double,
    var charge: Double
): Serializable {

    constructor(name: String, family: Family): this(name, family, Random.nextDouble(), Random.nextDouble(), Random.nextDouble())

    enum class Family {
        QUARK, LEPTON, GAUGE_BOSON, SCALAR_BOSON, ANTIPARTICLE
    }
}
