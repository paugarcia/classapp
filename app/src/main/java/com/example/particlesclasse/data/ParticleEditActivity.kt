package com.example.particlesclasse.data

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.particlesclasse.data.Particles.PARTICLES_FILENAME
import com.example.particlesclasse.databinding.ActivityParticleEditBinding
import java.io.ObjectOutputStream

class ParticleEditActivity : AppCompatActivity() {

    lateinit var binding: ActivityParticleEditBinding
    var particle: Particle? = null

    companion object {
        const val EXTRA_PARTICLE_ID = "EXTRA_PARTICLE_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityParticleEditBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val particleId = intent.extras?.getInt(EXTRA_PARTICLE_ID) ?: return
        particle = Particles[particleId]

        binding.particleNameInput.setText(particle?.name)
        binding.particleMassInput.setText(particle?.mass.toString())
        binding.particleChargeInput.setText(particle?.charge.toString())
        binding.particleSpinInput.setText(particle?.spin.toString())
    }

    override fun onPause() {
        super.onPause()

        particle?.name = binding.particleNameInput.text.toString()
        particle?.mass = binding.particleMassInput.text.toString().toDoubleOrNull() ?: 0.0
        particle?.charge = binding.particleChargeInput.text.toString().toDoubleOrNull() ?: 0.0
        particle?.spin = binding.particleSpinInput.text.toString().toDoubleOrNull() ?: 0.0

        Persistence(this).saveData()
    }
}