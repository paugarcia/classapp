package com.example.particlesclasse

import android.os.Bundle
import android.view.DragEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import androidx.appcompat.app.AppCompatActivity
import com.example.particlesclasse.databinding.ActivityDragAndDropBinding
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlin.reflect.safeCast


class DragAndDropActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDragAndDropBinding

    private val chipsLabels = listOf(
        "Blue supergiant", "Sun-Like Star", "Red Dwarf", "Brown Dwarf", "Red Giant",// 1st phase
        "Supernova", "Blackhole", "Neutron Star", "White Dwarf", // 2nd phase
        "Neutrino", "Chupa-chups", // Not star life
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDragAndDropBinding.inflate(layoutInflater)
        setContentView(binding.root)

        chipsLabels.forEach {
            val chip = Chip(this)
            chip.text = it

            chip.setOnLongClickListener {
                val shadow = View.DragShadowBuilder(chip)
                chip.startDragAndDrop(null, shadow, chip, 0)
                true
            }

            binding.chipGroup1.addView(chip)
        }


        binding.chipGroup2.setOnDragListener(moveDragListener)
        binding.chipGroup1.setOnDragListener(moveDragListener)

        binding.removingIcon.alpha = 0f
        binding.removingIcon.setOnDragListener(removeDragListener)

    }

    private val moveDragListener = View.OnDragListener { objectiveView, event ->

//        val movingChip = event.localState as Chip
        val movingChip = Chip::class.safeCast(event.localState) ?: return@OnDragListener false

        when (event.action) {
            DragEvent.ACTION_DRAG_STARTED -> {
                movingChip.setTextColor(getColor(R.color.transparent))
                movingChip.setChipStrokeColorResource(R.color.black)
                movingChip.chipStrokeWidth = 4f
//                movingChip.setBackgroundResource(R.color.transparent)
            }

            DragEvent.ACTION_DROP -> {
                val parent = ChipGroup::class.safeCast(movingChip.parent)
                parent?.removeView(movingChip)

                ChipGroup::class.safeCast(objectiveView)?.addView(movingChip)
            }

            DragEvent.ACTION_DRAG_ENDED -> {
                movingChip.setTextColor(getColor(R.color.black))
                movingChip.chipStrokeWidth = 0f
//                movingChip.setBackgroundResource(R.color.transparent)
            }
        }


        true
    }

    private val removeDragListener = View.OnDragListener { objectiveView, event ->

        val movingChip = Chip::class.safeCast(event.localState) ?: return@OnDragListener false

        when (event.action) {
            DragEvent.ACTION_DRAG_STARTED -> {
                binding.removingIcon.animate().alpha(1f)
            }

            DragEvent.ACTION_DRAG_ENTERED -> {
                binding.removingIcon.animate().scaleX(1.6f).scaleY(1.6f)
            }

            DragEvent.ACTION_DRAG_EXITED -> {
                binding.removingIcon.animate().scaleX(1f).scaleY(1f)
            }

            DragEvent.ACTION_DROP -> {
                val parent = ChipGroup::class.safeCast(movingChip.parent)
                parent?.removeView(movingChip)
            }

            DragEvent.ACTION_DRAG_ENDED -> {
                binding.removingIcon.animate().scaleX(0f).scaleY(0f).withEndAction {
                    binding.removingIcon.alpha = 0f
                    binding.removingIcon.scaleX = 1f
                    binding.removingIcon.scaleY = 1f
                }

            }
        }


        true
    }
}