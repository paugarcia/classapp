package com.example.particlesclasse

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.particlesclasse.data.Particles
import com.example.particlesclasse.data.Persistence
import com.example.particlesclasse.databinding.ActivityMainBinding
import com.example.particlesclasse.nasa.NasaSearchEngineActivity
import com.example.particlesclasse.retrofit.RetrofitActivity
import com.example.particlesclasse.ui.ParticlesListActivity

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.goParticlesList.setOnClickListener {
            startActivity(Intent(this, ParticlesListActivity::class.java))
        }

        binding.goDragAndDrop.setOnClickListener {
            startActivity(Intent(this, DragAndDropActivity::class.java))
        }

        binding.goLogin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        binding.goRetrofit.setOnClickListener {
            startActivity(Intent(this, RetrofitActivity::class.java))
        }

        binding.goNasaSearchEngine.setOnClickListener {
            startActivity(Intent(this, NasaSearchEngineActivity::class.java))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> startActivity(Intent(this, SettingsActivity::class.java))
            R.id.action_reset -> resetParticles()
        }


        return super.onOptionsItemSelected(item)
    }

    fun resetParticles() {
        Particles.resetData()

        val pers = Persistence(this)
        pers.saveData()


    }
}