package com.example.particlesclasse.retrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.particlesclasse.databinding.ActivityRetrofitBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class RetrofitActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRetrofitBinding

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.chucknorris.io/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRetrofitBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getQuote()

        binding.quoteRefreshButton.setOnClickListener {
            getQuote()
        }

    }

    private fun getQuote() {
        val call = retrofit.create(ChuckNorrisAPI::class.java).getRandomQuote()

        println("New Quote: ")
        binding.progressBar.visibility = View.VISIBLE

        call.enqueue(object : Callback<Quote> {

            override fun onResponse(call: Call<Quote>, response: Response<Quote>) {
                println(response.body())

                binding.quote.text = response.body()?.quote ?: "Quote not found :("
                binding.quoteDate.text = response.body()?.date ?: ""

                binding.progressBar.visibility = View.GONE
            }

            override fun onFailure(call: Call<Quote>, t: Throwable) {
                println(t)

                Toast.makeText(this@RetrofitActivity, "Error getting quote", Toast.LENGTH_SHORT).show()
                binding.progressBar.visibility = View.GONE
            }
        })
    }
}