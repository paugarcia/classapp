package com.example.particlesclasse.retrofit

import retrofit2.Call
import retrofit2.http.GET

interface ChuckNorrisAPI {

    @GET("jokes/random")
    fun getRandomQuote(): Call<Quote>
}