package com.example.particlesclasse.retrofit

import com.google.gson.annotations.SerializedName

data class Quote(@SerializedName("value") val quote: String,
                 @SerializedName("updated_at") val date: String)