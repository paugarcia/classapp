package com.example.particlesclasse

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Patterns
import android.widget.Toast
import com.example.particlesclasse.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.emailInput.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val email = binding.emailInput.text

                if (!validateEmail(email))
                    binding.emailInput.error = "Incorrect email"
            }
        }

        binding.passwordInput.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val password = binding.passwordInput.text

                if (!validatePassword(password))
                    binding.passwordInput.error = "Incorrect email"
            }
        }

        binding.loginButton.setOnClickListener {
            if (validatePassword(binding.passwordInput.text) && validateEmail(binding.emailInput.text)) {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                Toast.makeText(this, "Login not succeed", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun validatePassword(password: Editable?): Boolean {
        return !password.isNullOrEmpty() && password.length >= 5
    }

    fun validateEmail(email: Editable?): Boolean {
        return if (email.isNullOrEmpty()) false
        else Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

}

