package com.example.particlesclasse.nasa.rest

import com.google.gson.annotations.SerializedName

data class NasaPhotosCollection(@SerializedName("collection") val collection: PhotosList) {
    data class PhotosList(@SerializedName("items") val photosList: List<InternalPhoto>)
    data class InternalPhoto(@SerializedName("data") val data: List<Data>, @SerializedName("links") val links: List<Link>)
    data class Data(val title: String, val description: String, var location: String?)
    data class Link(@SerializedName("href") val link: String)

    fun getPhotosList(): List<NasaPhoto> {
        return collection.photosList.mapNotNull {
            if (it.data.isNullOrEmpty() || it.links.isNullOrEmpty())
                return@mapNotNull null

            val data = it.data[0]
            NasaPhoto(data.title, data.description, data.location, it.links[0].link)
        }
    }
}