package com.example.particlesclasse.nasa

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SearchView
import android.widget.Toast
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.example.particlesclasse.databinding.ActivityNasaSearchEngineBinding
import com.example.particlesclasse.nasa.rest.APINasa
import com.example.particlesclasse.nasa.rest.NasaPhotosCollection
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NasaSearchEngineActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNasaSearchEngineBinding
    private val adapter = NasaPhotosListAdapter(this@NasaSearchEngineActivity)

    private val theOutside = Retrofit.Builder()
        .baseUrl("https://images-api.nasa.gov/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityNasaSearchEngineBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.nasaRecyclerView.adapter = adapter

        binding.nasaSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String?): Boolean =  true
            override fun onQueryTextSubmit(query: String?): Boolean {
                binding.nasaSearch.clearFocus()
                binding.nasaRecyclerView.scrollToPosition(0)
                if (query != null)
                    preformSearch(query)
                return true
            }
        })

        preformSearch("Aurora")
    }

    fun preformSearch(query: String) {
        val call = theOutside.create(APINasa::class.java).getPhotosList(query)
        binding.nasaProgress.isVisible = true

        call.enqueue(object : Callback<NasaPhotosCollection> {

            override fun onResponse(
                call: Call<NasaPhotosCollection>,
                response: Response<NasaPhotosCollection>
            ) {
                val res = response.body() ?: return
                adapter.updatePhotosList(res.getPhotosList())
                binding.nasaProgress.isGone = true
            }

            override fun onFailure(call: Call<NasaPhotosCollection>, t: Throwable) {
                binding.nasaProgress.isGone = true
                Toast.makeText(this@NasaSearchEngineActivity, t.toString(), Toast.LENGTH_SHORT).show()
            }

        })
    }
}