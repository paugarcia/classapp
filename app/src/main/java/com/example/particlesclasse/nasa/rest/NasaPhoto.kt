package com.example.particlesclasse.nasa.rest

data class NasaPhoto(val title: String, val description: String, val location: String?, val link: String)