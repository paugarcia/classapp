package com.example.particlesclasse.nasa.rest

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface APINasa {

    @GET("/search")
    fun getPhotosList(@Query("q") searchString: String): Call<NasaPhotosCollection>


}