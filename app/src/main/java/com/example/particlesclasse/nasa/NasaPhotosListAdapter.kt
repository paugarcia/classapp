package com.example.particlesclasse.nasa

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.particlesclasse.R
import com.example.particlesclasse.databinding.ItemNasaImageBinding
import com.example.particlesclasse.nasa.rest.NasaPhoto
import com.squareup.picasso.Picasso

class NasaPhotosListAdapter(val context: Context) : RecyclerView.Adapter<NasaPhotosListAdapter.NasaViewHolder>() {

    private var photosList: List<NasaPhoto> = listOf()

    inner class NasaViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemNasaImageBinding.bind(view)

        val image = binding.nasaItemImage
        val title = binding.nasaItemTitle
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NasaViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = ItemNasaImageBinding.inflate(inflater, parent, false).root

        return NasaViewHolder(view)
    }

    override fun onBindViewHolder(holder: NasaViewHolder, position: Int) {
        val element = photosList[position]

        holder.title.text = element.title

//        Picasso.get()
//            .load(element.link)
//            .into(holder.image)

        Picasso.Builder(context)
            .build()
            .load(element.link)
            .placeholder(R.drawable.image_placeholder)
            .into(holder.image)
    }

    override fun getItemCount(): Int {
        return photosList.size
    }

    fun updatePhotosList(photosList: List<NasaPhoto>) {
        this.photosList = photosList
        notifyDataSetChanged()
    }
}